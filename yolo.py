#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time : 2021/12/19 下午 05:49
# @Author : Aries
# @Site : 
# @File : yolo.py
# @Software: PyCharm


import os
import cv2
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np

if __name__ == '__main__':
    route = os.getcwd()
    # 訓練模型
    # os.system(f"cd yolov5 && python train.py --img 416 --batch 16 --epochs 25 --data {route}/datasets/worker/data.yaml --weights yolov5s.pt --cache")
    # 測試模型
    # os.system(f"cd yolov5 && python detect.py --weights runs/train/exp/weights/best.pt --img 416 --conf 0.1 --source {route}/datasets/worker/test/images --save-txt")
    # 使用自己的圖片

    n = 416
    # os.system(f"cd yolov5 && python detect.py --weights runs/train/exp/weights/best.pt --img {n} --conf 0.7 --source {route}/pictures/workers/test.jpg --save-txt")
    img = cv2.imread('pictures/workers/test.jpg')
    x = int(n * 0.391827)
    y = int(n * 0.28726)
    w = int(n * 0.0961538 /2)
    h = int(n * 0.185096 /2)
    crop_img = img[y - h:y + h, x - w:x + w]
    crop_img= Image.fromarray(cv2.cvtColor(crop_img,cv2.COLOR_BGR2RGB))
    plt.imshow(crop_img)
    plt.show()