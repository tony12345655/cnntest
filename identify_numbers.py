from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.utils import np_utils
from keras.datasets import mnist
import cv2
import numpy as np
import matplotlib.pyplot as plt


def train_model():
    # 將影像特徵以reshape轉為60000*28*28*1的四維矩陣後再轉乘float
    (imgTrain, labelTrain), (imgTest, labelTest) = mnist.load_data()
    Train4D = imgTrain.reshape(imgTrain.shape[0], 28, 28, 1).astype('float32')

    # 將影像特徵標準化，可提昇模型預測的準確度，並且更快收斂
    Test4D = imgTest.reshape(imgTest.shape[0], 28, 28, 1).astype('float32')  # 資料型別轉換
    Train4D_normalize = Train4D / 255  # 除以255（因為圖像的像素點介於0~255之間）
    Test4D_normalize = Test4D / 255

    # 將訓練和測試的label,進行Onehot encoding轉換
    TrainOneHot = np_utils.to_categorical(labelTrain)
    TestOneHot = np_utils.to_categorical(labelTest)

    # 建立一個Sequential模型，後續add即可加入模型
    model = Sequential()
    # 建立卷積層1
    model.add(Conv2D(filters=32,  # 建立32個濾鏡
                     kernel_size=(4, 4),  # 每個濾鏡4*4的大小
                     padding='same',  # 讓卷積運算不會影響圖的大小
                     input_shape=(28, 28, 1),  # 輸入影像的形狀(影像長，影像寬,影像色階)
                     activation='relu'))  # 定義激活函數為RELU
    # 建立池化層1
    model.add(MaxPooling2D(pool_size=(2, 2)))  # 縮減取樣，縮小為32個16*16的影像

    # 建立卷積層2
    model.add(Conv2D(filters=64,  # 建立64個濾鏡
                     kernel_size=(4, 4),  # 每個濾鏡4*4的大小
                     padding='same',  # 讓卷積運算不會影響圖的大小
                     activation='relu'))  # 定義激活函數為RELU
    # 建立池化層2
    model.add(MaxPooling2D(pool_size=(2, 2)))  # 縮減取樣，縮小為64個32*32的影像

    # 加入Dropout功能 每次訓練迭代時，隨機放棄25%的神經元，避免發生overfitting(過度學習)
    model.add(Dropout(0.25))
    # 建立平坦層
    model.add(Flatten())
    # 建立隱藏層
    model.add(Dense(1500, activation='relu'))
    # 建立輸出層
    model.add(Dense(10, activation='softmax'))
    # 定義訓練的方法     # 優化器='adam優化方法',評估模型的方法='accuracy準確率')
    model.compile(loss='categorical_crossentropy',  # (損失函數='分類交叉熵函數'
                  optimizer='adam', metrics=['accuracy'])

    # 使用model.fit進行訓練，訓練過程會存在trainHistory變數
    trainHistory = model.fit(x=Train4D_normalize,  # 訓練影像的特徵值
                             y=TrainOneHot,  # 訓練影像真實值
                             validation_split=0.2,  # 設定訓練和驗證資料的比例
                             epochs=10, batch_size=500,  # 訓練週期10次，每次500筆資料
                             verbose=1)  # 顯示訓練過程
    # verbose = 0 為不在標準輸出流輸出日誌資訊
    # verbose = 1 為輸出進度條記錄
    # verbose = 2 為每個epoch輸出一行記錄

    scores = model.evaluate(Test4D_normalize, TestOneHot)
    # 評估模型準確率  將資料儲存在scores裡(測試影像特徵值，測試影像真實值)
    print('準確度: ' + str(scores[1]))

    # 儲存模型
    model.save('models/number_model.h5')


def test():
    # 辨識數字模型
    model = load_model('models/number_model.h5')
    img = cv2.imread('pictures/numbers/7.png')
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # 黑白相反
    img = np.abs(img - 255)
    res = model.predict(img.reshape(1, 28, 28, 1))
    print(np.argmax(res))
    picture = img.reshape(28, 28)
    plt.imshow(picture, cmap='binary')
    plt.show()


if __name__ == '__main__':
    train_model()
    test()
